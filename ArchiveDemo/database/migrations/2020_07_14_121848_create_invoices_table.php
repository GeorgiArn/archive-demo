<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->foreignId('id')->unique();
            $table->double('taxBase')->nullable();
            $table->foreignId('currencyId')->nullable();
            $table->boolean('vat')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();

            $table
                ->foreign('id')
                ->references('id')
                ->on('files')
                ->cascadeOnDelete();

            $table
                ->foreign('currencyId')
                ->references('id')
                ->on('currencies')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}

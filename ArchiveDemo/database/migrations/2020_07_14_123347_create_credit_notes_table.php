<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_notes', function (Blueprint $table) {
            $table->foreignId('id')->unique();
            $table->foreignId('paymentMethodId')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();

            $table
                ->foreign('id')
                ->references('id')
                ->on('files')
                ->cascadeOnDelete();

            $table
                ->foreign('paymentMethodId')
                ->references('id')
                ->on('payment_methods')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_notes');
    }
}

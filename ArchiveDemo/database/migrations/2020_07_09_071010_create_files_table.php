<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->integer('number')->nullable();
            $table->date('date')->nullable();
            $table->foreignId('typeId')->nullable();
            $table->string('path');
            $table->foreignId('clientId');
            $table->boolean('isFilled')->nullable();
            $table->timestamps();

            $table->foreign('clientId')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();

            $table->foreign('typeId')
                ->references('id')
                ->on('file_types')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}

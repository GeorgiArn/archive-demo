<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(FileTypeSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(PaymentMethodSeeder::class);
    }
}

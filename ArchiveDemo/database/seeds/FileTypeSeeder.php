<?php

use Illuminate\Database\Seeder;

class FileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\FileType::class)->create(['name'=>'Invoice']);
        factory(\App\FileType::class)->create(['name'=>'Bank Statement']);
        factory(\App\FileType::class)->create(['name'=>'Credit Note']);
    }
}

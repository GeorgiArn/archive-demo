<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Currency::class)->create(['name' => 'EUR']);
        factory(\App\Currency::class)->create(['name' => 'USD']);
        factory(\App\Currency::class)->create(['name' => 'BGN']);
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestController@show')->name('welcome');

Route::get('/clients/datatable', 'Admin\ClientController@datatable')->name('clients.datatable');
Route::resource('clients', 'Admin\ClientController', ['only' => ['index', 'store', 'show']]);

Route::resource('files', 'Admin\FileController', ['only' => ['edit', 'update']]);
Route::resource('files', 'Client\FileController', ['only' => ['index', 'create', 'store']]);

Route::post('/files/{path}/download', 'FileController@download')->name('files.download');
Route::get('/files/datatable/{userId}', 'FileController@datatable')->name('files.datatable');

Auth::routes();

<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Requests\DownloadFile;
use App\User;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function download(DownloadFile $request)
    {
        $file = File::where('path', $request->route('path'))->first();
        $client = $file->client->companyName;

        $ext = pathinfo($file->path, PATHINFO_EXTENSION);

        return Response::download("clientFiles/".$file->path, "$client-$file->id-archive-info.$ext");
    }

    public function datatable()
    {
        $this->authorize('index-file', $_GET['userId']);

        $user = User::find($_GET['userId']);

        $files = $user->files()->with('type');

        return DataTables::of($files)
            ->addColumn('action', function ($file) {
                return view('file.partials.actions', compact('file'));
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}

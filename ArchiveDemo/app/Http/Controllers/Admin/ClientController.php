<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClient;
use App\Mail\ContactClient;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdmin');
    }

    public function index()
    {
        return view('admin.client.index');
    }

    public function datatable()
    {
        $clients = auth()->user()->clients();

        return DataTables::of($clients)
            ->addColumn('unfilled-files', function ($client) {
                return $client->unfilledFilesCount();
            })
            ->addColumn('action', function ($client) {
                return view('admin.client.partials.view', compact('client'));
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(StoreClient $request)
    {
        $attributes = $request->validated();

        // Generate random password for client
        $clientPass = Str::random(8);

        // Create client
        $user = User::create([
            'companyName' => $attributes['company-name'],
            'uic' => $attributes['uic'],
            'email' => $attributes['email'],
            'password' => Hash::make($clientPass),
            'accountingAdminId' => auth()->user()->id
        ]);
        $user->assignRole(Role::find(1));

        // Send email to client with password
        Mail::to($attributes['email'])
            ->send(new ContactClient($clientPass, auth()->user()));

        return back();
    }

    public function show(User $client) {
        $this->authorize('show-client', $client);

        return view('admin.client.show', compact('client'));
    }


}

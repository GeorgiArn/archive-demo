<?php

namespace App\Http\Controllers\Admin;

use App\BankStatement;
use App\CreditNote;
use App\Currency;
use App\File;
use App\FileType;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateFile;
use App\Invoice;
use App\PaymentMethod;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkAdmin');
    }

    public function edit(File $file)
    {
        $this->authorize('update-file', $file);

        $file = $file->with('invoice', 'bankStatement', 'creditNote', 'client')->get()->where('id', $file->id)->first();

        $fileTypes = FileType::all();
        $paymentMethods = PaymentMethod::all();
        $currencies = Currency::all();

        return view('admin.file.edit', compact( 'file', 'fileTypes', 'paymentMethods', 'currencies'));
    }

    public function update(File $file, UpdateFile $request)
    {
        $attributes = $request->validated();
        $attributes['isFilled'] = array_key_exists('isFilled', $attributes) ? true : false;

        $attributes['typeId'] === "1" ? Invoice::updateOrCreate(
            ['id' => $file->id],
            ['id' => $file->id,
            'taxBase' => $attributes['tax-base'],
            'currencyId' => $attributes['currencyId'],
            'vat' => array_key_exists('vat', $attributes) && $attributes['vat'] === "on" ? true : false,
            'price' => $attributes['price']]
        ) : Invoice::where('id', $file->id)->delete();

        $attributes['typeId'] === "2" ? BankStatement::updateOrCreate(
            ['id' => $file->id],
            ['id' => $file->id,
            'bankName' => $attributes['bank-name'],
            'iban' => $attributes['iban']]
        ) : BankStatement::where('id', $file->id)->delete();;

        $attributes['typeId'] === "3" ? CreditNote::updateOrCreate(
            ['id' => $file->id],
            ['id' => $file->id,
            'paymentMethodId' => $attributes['paymentMethodId'],
            'note' => $attributes['note']]
        ) : CreditNote::where('id', $file->id)->delete();;

        $file->update($attributes);

        return redirect(route('clients.show', $file->client->id));
    }
}

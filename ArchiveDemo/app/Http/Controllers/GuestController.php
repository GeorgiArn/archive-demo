<?php

namespace App\Http\Controllers;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function show()
    {
        return view('guest.show');
    }
}

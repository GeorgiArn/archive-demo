<?php

namespace App\Http\Controllers\Client;

use App\File;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkClient');
    }

    public function index()
    {
        return view('file.index');
    }

    public function create()
    {
        return view('file.create');
    }

    public function store(Request $request)
    {
        $file = $request->file('file');

        File::create([
            'path' => Storage::disk('public')->put('/', $file),
            'clientId' => auth()->user()->id
        ]);
    }

}

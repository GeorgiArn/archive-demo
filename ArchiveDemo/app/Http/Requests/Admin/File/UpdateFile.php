<?php

namespace App\Http\Requests;

use App\File;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateFile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update-file', $this->route('file'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // General Validations
            'number' => ['integer'],
            'date' => ['date', 'date_format:Y-m-d'],
            'typeId' => ['exists:file_types,id'],
            'isFilled' => [],

            // Invoice Validations
            'tax-base' => ['numeric'],
            'currencyId' => ['exists:currencies,id'],
            'vat' => [],
            'price' => ['numeric'],

            // Bank Statement Validations
            'bank-name' => ['string', 'max:255'],
            'iban' => ['string', 'max:255'],

            // Credit Note Validations
            'note' => ['string', 'max:255'],
            'paymentMethodId' => ['exists:payment_methods,id']
        ];
    }
}

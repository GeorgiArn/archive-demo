<?php

namespace App\Http\Middleware;

use Closure;

class ClientAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user() && auth()->user()->isClient()) {
            return $next($request);
        }

        if (auth()->user()) {
            return redirect(route('clients.index'));
        }

        return redirect(route('welcome'));
    }
}

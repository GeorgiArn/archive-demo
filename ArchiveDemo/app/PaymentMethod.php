<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    // Relations
    public function creditNote()
    {
        return $this->hasMany(CreditNote::class, 'paymentMethodId');
    }
}

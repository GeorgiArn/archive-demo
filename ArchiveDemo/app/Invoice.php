<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'id', 'taxBase', 'currencyId', 'vat', 'price'
    ];

    // Relations
    public function file()
    {
        return $this->belongsTo(File::class, 'id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currencyId');
    }
}

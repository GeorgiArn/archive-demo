<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankStatement extends Model
{
    protected $fillable = [
        'id', 'bankName', 'iban'
    ];

    // Relations
    public function file()
    {
        return $this->belongsTo(File::class, 'id');
    }
}

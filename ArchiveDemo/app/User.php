<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'companyName', 'uic', 'accountingAdminId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relations
    public function accountingAdmin()
    {
        return $this->belongsTo(User::class,
            'accountingAdminId');
    }

    public function clients()
    {
        return $this->hasMany(User::class, 'accountingAdminId');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class,
            'users_roles',
            'userId',
            'roleId');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'clientId');
    }

    // Helper functions
    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    public function isAdmin()
    {
        return (bool) $this->roles()->where('roleId', 2)->count();
    }

    public function isClient()
    {
        return (bool) $this->roles()->where('roleId', 1)->count();
    }

    public function unfilledFilesCount()
    {
        return $this->files->where('isFilled', false)->count();
    }
}

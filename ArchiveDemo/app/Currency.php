<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    // Relations
    public function invoice()
    {
        return $this->hasMany(Invoice::class, 'currencyId');
    }
}

<?php

namespace App\Policies;

use App\File;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

class FilePolicy
{
    use HandlesAuthorization;

    public function index(User $currUser, string $userId)
    {
        $isFilesOwner = $currUser->isClient() && $currUser->id == $userId;
        $isAdminAndUserIsClient = (bool) $currUser->isAdmin() && $currUser->clients()->where('id', $userId)->count();

        return  $isFilesOwner || $isAdminAndUserIsClient;
    }

    public function download(User $currUser, string $path)
    {
        $isAdminAndClientOwnsFile = (bool) User::whereHas('clients.files', function (Builder $query) use ($path) {
            $query
                ->where('users.id', '=', auth()->user()->id)
                ->where('files.path', 'like', "%$path%");
        })->get()->count();

        $isFileOwner = (bool) $currUser->files()->where('path', $path)->count();

        return $isAdminAndClientOwnsFile || $isFileOwner;
    }

    public function update(User $currUser, File $file)
    {
        $isAdminAndClientOwnsFile = (bool) User::whereHas('clients.files', function (Builder $query) use ($currUser, $file) {
            $query
                ->where('users.id', '=', $currUser->id)
                ->where('files.id', '=', $file->id);
        })->get()->count();

        return $isAdminAndClientOwnsFile;
    }
}

<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    public function store(User $currUser)
    {
        return $currUser->isAdmin();
    }

    public function show(User $currUser, User $client)
    {
        return (bool) $currUser->isAdmin() && $currUser->clients()->where('id', $client->id)->count();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditNote extends Model
{
    protected $fillable = [
        'id', 'paymentMethodId', 'note'
    ];

    // Relations
    public function file()
    {
        return $this->belongsTo(File::class, 'id');
    }

    public function paymentMethod()
    {
        return $this->belongsToMany(PaymentMethod::class, 'paymentMethodId');
    }

}

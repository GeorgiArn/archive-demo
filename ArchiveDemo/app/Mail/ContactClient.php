<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactClient extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $accountingAdmin;

    /**
     * Create a new message instance.
     *
     * @param $password
     * @param $accountingAdmin
     */
    public function __construct($password, $accountingAdmin)
    {
        $this->password = $password;
        $this->accountingAdmin = $accountingAdmin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.client-mail');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    // Relations
    public function files()
    {
        return $this->hasMany(File::class, 'typeId');
    }
}

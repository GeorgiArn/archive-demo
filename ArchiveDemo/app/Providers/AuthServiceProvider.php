<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('store-client', 'App\Policies\ClientPolicy@store');
        Gate::define('show-client', 'App\Policies\ClientPolicy@show');

        Gate::define('index-file', 'App\Policies\FilePolicy@index');
        Gate::define('update-file', 'App\Policies\FilePolicy@update');
        Gate::define('download-file', 'App\Policies\FilePolicy@download');
    }
}

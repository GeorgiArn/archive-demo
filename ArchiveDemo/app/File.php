<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        "number", "date", "typeId", "path", "clientId", "isFilled"
    ];

    // Relations
    public function client()
    {
        return $this->belongsTo(User::class, 'clientId');
    }

    public function type()
    {
        return $this->belongsTo(FileType::class, 'typeId');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'id');
    }

    public function bankStatement()
    {
        return $this->hasOne(BankStatement::class, 'id');
    }

    public function creditNote()
    {
        return $this->hasOne(CreditNote::class, 'id');
    }

    // Helper functions
    public function getFilePath()
    {
        return $this->path;
    }
}

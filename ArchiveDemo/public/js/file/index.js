$(document).ready(function () {
    $('#files-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: '/files/datatable/{userId}',
            type: 'GET',
            data: { 'userId': $('#user-id').val() },
        },
        columns: [
            {
                data: "number",
                name: "number"
            },
            {
                data: "date",
                name: "date"
            },
            {
                data: "type.name",
                name: "type.name"
            },
            {
                data: "action",
                name: "action",
                orderable: false
            }
        ],
        columnDefs: [{
            targets: '_all',
            defaultContent: "Not registered, yet"
        }],
        scrollY: '200px'
    });
});

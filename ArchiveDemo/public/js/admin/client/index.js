$(document).ready(function () {
    $('#clients-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/clients/datatable",
            type: 'GET'
        },
        columns: [
            {
                data: "companyName",
                name: "companyName"
            },
            {
                data: "email",
                name: "email"
            },
            {
                data: "uic",
                name: "uic"
            },
            {
                data: "unfilled-files",
                name: "unfilled-files"
            },
            {
                data: "action",
                name: "action",
                orderable: false
            }
        ],
        scrollY: "200px"
    })
});

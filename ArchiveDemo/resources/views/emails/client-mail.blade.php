<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Archive Info</title>
</head>
<body>
<div>
    <h2>Successful Registration!</h2>
    <p>You have been successfully added to Archive Info as a client by {{$accountingAdmin->companyName}}!</p>
    <p>Here is your password: {{$password}}</p>
</div>
</body>
</html>




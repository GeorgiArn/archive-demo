@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/file/index.js') }}"></script>
@endsection

@section('content')
    <div class="col-md-10">
        <div class="card m2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        Your Files
                    </div>
                    <div class="text-md-right">
                        <a class="btn btn-primary" href="{{route('files.create')}}">Add Files</a>
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <input type="hidden" id="user-id" value="{{auth()->user()->id}}">
                <table id="files-table" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

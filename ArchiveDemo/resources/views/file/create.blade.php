@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="card m2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        Upload Files
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <form method="post" action="{{route('files.store')}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                    @csrf
                </form>
            </div>
        </div>
    </div>
@endsection

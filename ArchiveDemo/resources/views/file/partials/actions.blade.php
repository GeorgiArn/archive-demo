<form method="POST" action="{{route('files.download', $file->getFilePath())}}">
    @csrf
    <input type="submit" value="Download">
</form>
@if(auth()->user()->isAdmin())
    <div class="mt-2">
        <a class="btn btn-info" href="{{route('files.edit', $file->id)}}">Edit</a>
    </div>
@endif

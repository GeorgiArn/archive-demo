@extends('layouts.app')

@section('content')
    <div class="col-md-10">
        <div class="card m2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        Edit {{$file->client->companyName}}'s file
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <form method="post" action="{{route('files.update', $file->id)}}">
                    @csrf
                    @method("PUT")
                    <div class="form-group row">
                        <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Number') }}</label>

                        <div class="col-md-6">
                            <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ $file->number }}" required autocomplete="number" autofocus>

                            @error('number')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <file-types :file="{{$file}}" :types="{{$fileTypes}}" :methods="{{$paymentMethods}}" :currencies="{{$currencies}}"></file-types>

                    <div class="form-group row">
                        <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                        <div class="col-md-6">
                            <input id="date" type="text" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ $file->date }}" required autocomplete="date">

                            @error('date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="isFilled" class="col-md-4 col-form-label text-md-right">Is Filled</label>

                        <div class="col-md-6">
                            <input id="isFilled" type="checkbox" class="form-control" name="isFilled" {{$file->isFilled ? 'checked' : ''}}>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Edit file</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection



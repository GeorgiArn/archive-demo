@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/file/index.js') }}"></script>
@endsection

@section('content')
    <div class="col-md-10">
        <div class="card m2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        {{$client->companyName}}'s data
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <p><b>Company Name:</b> {{$client->companyName}}</p>
                <p><b>UIC:</b> {{$client->uic}}</p>
                <p><b>Email:</b> {{$client->email}}</p>
                <input type="hidden" id="user-id" value="{{$client->id}}">
                <table id="files-table" class="display mt-3" style="width:100%">
                    <thead>
                    <tr>
                        <th>Number</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

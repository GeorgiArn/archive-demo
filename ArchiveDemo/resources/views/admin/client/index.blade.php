@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/admin/client/index.js') }}"></script>
@endsection

@section('content')
    <div class="col-md-10">
        <div class="card m2">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        Your Clients
                    </div>
                    <div class="text-md-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addClientForm">Add client</button>
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <table id="clients-table" class="display" style="width:100%">
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Email</th>
                        <th>UIC</th>
                        <th>Unfilled Files</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @include('admin.client.create')
@endsection

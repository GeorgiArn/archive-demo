@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/guest/show.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Welcome to Archive Info!
            </div>
        </div>
    </div>
@endsection
